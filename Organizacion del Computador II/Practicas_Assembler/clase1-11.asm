%include "io.inc"
 
section .data
 
f1 dd 14.0 , 3.0 , 17.0, 4999999999.0
 
e1 dq 19, 67, 124, -2147483647
 
section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    ;write your code here
   
    movdqu xmm1, [e1]
   
    CVTDQ2PS xmm0,xmm1
   
    mulps xmm0, [f1]
   
    movups [f1] , xmm0
   
    xor eax, eax
    ret