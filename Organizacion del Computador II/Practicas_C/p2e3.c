#include <stdio.h>

typedef struct {
	long long x0;
	long long x1;
} superlong;

int main( int argc, const char* argv[] )
{
	return 0;
}

int func_a(long a, long b);
int func_b(long a, long* b);
int* func_c(long a, long b);
void func_d(short int a, long b);
void func_e(long int a, char b, int c);
void func_f(superlong a, char* b);
void func_g(char b, superlong a, int c);
