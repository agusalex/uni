package binyHex;

import java.util.Scanner;

public class Funciones {
		
	static String escanear(){
		Scanner scanobj = new Scanner(System.in);
		scanobj.close();
		return scanobj.nextLine();}
	
	
	static int escanearInt(){ 
			Scanner objetoscan= new Scanner(System.in); 
			int escaneado= objetoscan.nextInt();
			objetoscan.close();
			return escaneado;}
	public static String numtoBin(int n){
		int base=2;
		int grupos=4;
		String cadena=" ";
		while (n>=base){
			
			if (n%base<base){cadena=String.valueOf(n%base)+cadena;}
			if (cadena.length()%(grupos+1)==0){cadena=' '+cadena;}
			n=n/base;}
		cadena='1'+cadena;
		while (cadena.length()%(grupos+1)!=0){
			cadena='0'+cadena;
		}
		cadena=' '+cadena;
		return cadena;}

	public static String numtoHex(int num){
		String hex="";
		int acum=0;
		int y=0;
		String binario=numtoBin(num);
		
		for (int x=binario.length()-2;x>=0;x--){
			if (binario.charAt(x)=='1'){
				acum=(int)(acum+ Math.pow(2, y));}
			if (binario.charAt(x)==' '){
				switch(acum){
					default : hex=acum+hex;break;
					case 10 : hex='A'+hex;break;
					case 11 : hex='B'+hex;break;
					case 12 : hex='C'+hex;break;
					case 13 : hex='D'+hex;break;
					case 14 : hex='E'+hex;break;
					case 15 : hex='F'+hex;break;}y=-1;acum=0;}y++;}
		    return hex;}
			
	
	}

