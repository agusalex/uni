def Perfecto(n):
    sumadivisores=0
    for i in range (1,(n//2)+1):
        if n%i==0:
            sumadivisores+=i

    if sumadivisores==n:
        return True
    else:
        return False

def Envidioso(n):
    if not Perfecto(n):
        for i in range (1,(n//2)+1):
            if n%i==0 and Perfecto(i):
                return True
    return False

print(Envidioso(18))