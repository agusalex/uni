import pygame,random
def salir():
    pygame.quit()
    quit()
def mensaje(msg,color,x,y):
    pantalla_text=fuente.render(msg, True, color)
    pantalla.blit(pantalla_text,[x,y])

def mensajecentrado(msg,color,pos):
    pantalla_text=fuente.render(msg, True, color)
    pantalla.blit(pantalla_text,[(ancho/2-(len(msg)*(tamfuente/5.75))),(largo/2)+pos])

#inicializar
pygame.init()
ancho=1024
largo=768
mitadtexto=[(ancho-120),(largo/2)]
pantalla = pygame.display.set_mode((ancho,largo))
#nombre
pygame.display.set_caption('Python')
#colores
blanco = (255,255,255)
turquesa =(93, 193, 185)
rojo=(255,0,0)
verdeosc=(0,153,0)
verdeclaro=(0,255,0)
azul=(0,0,255)
negro=(0,0,0)
colores=[verdeclaro,rojo,verdeosc,azul,blanco,negro]
#Variables
clock=pygame.time.Clock()
Gameover=False
GameExit=False
unlockExit=False
#
x=False
y=True
signox=1
signoy=-1
#diseno
color=colores[1]
colorpantalla=2
tamfuente=30
fuente=pygame.font.SysFont(None,tamfuente)
#Dinamica
movex=ancho/2
movey=largo/2
cuadrax=15
velocidad=2.5
FPS=20
manzanax=round(random.randrange(0,ancho)/15.0)*15.0
manzanay=round(random.randrange(0,largo)/15.0)*15.0
manzanasizex=cuadrax

#Loop del juego

while not GameExit:
    #MENU
    while Gameover and not GameExit:
        mensajecentrado("Game Over",rojo,-80)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                GameExit=True
            if event.type == pygame.KEYDOWN:
                if event.key==pygame.K_RETURN and unlockExit==True:
                    GameExit=True
                elif event.key == pygame.K_RETURN:
                    Gameover=False
                elif event.key == pygame.K_DOWN:
                    unlockExit=True #mover el cuadradito del menu

        mensajecentrado("Play Again",turquesa,0)
        mensajecentrado("Exit",turquesa,50)

        pygame.display.update()
        clock.tick(FPS-50)

#Reinitialize
    movex=ancho/2
    movey=largo/2
    manzanax=round(random.randrange(0,ancho-cuadrax)/30.0)*30.0
    manzanay=round(random.randrange(0,largo-cuadrax)/30.0)*30.0
    colorpantalla=2
    unlockExit=False
    while not Gameover:

        for event in pygame.event.get():
            #Quit
            if event.type == pygame.QUIT:
                salir()
            #CONTROLES

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    signox=-1
                    x=True
                    y=False
                if event.key == pygame.K_RIGHT:
                    signox=1
                    x=True
                    y=False
                if event.key == pygame.K_UP:
                    signoy=-1
                    y=True
                    x=False
                if event.key == pygame.K_DOWN:
                    signoy=1
                    y=True
                    x=False
        #Gameover
        if movex<=0 or movex>=ancho-cuadrax or movey<=0 or movey>=largo-cuadrax:
            colorpantalla=5
            Gameover=True
        #Movimiento
        elif x:
            movex+=velocidad*signox
        elif y:
            movey+=velocidad*signoy
##        if abs(movex-manzanax)<500:
##            movex==manzanax
##            movey==manzanay
##        if abs(movey-manzanay)<500:
##            movey==manzanay
##            movex==manzanax

        pantalla.fill(colores[colorpantalla])
        #OBJETOS
        manzana=pygame.draw.rect(pantalla,color,[manzanax,manzanay,manzanasizex,manzanasizex])
        cudrado=pygame.draw.rect(pantalla,azul,[round(movex/15)*15,round(movey/15)*15,cuadrax,cuadrax])

        pygame.display.update()
        clock.tick(FPS)
salir()


