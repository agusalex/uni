package f;

import java.util.LinkedList;

public class Arbol {
	public Nodo Raiz;
	public int cant;
	
	
	public Arbol (Nodo Raize){
		this.Raiz=Raize;
		
	}
	public Arbol (){
	
		
	}
public boolean Buscar(int load){
	Nodo nodito=new Nodo(load);
	int rta=this.Raiz.Buscar(nodito);
	if(rta==0){
		return false;
	}
	return true;
	
}


public int CantNodos(){
	
	return cant;
}
 
public void eliminar(int e){
	
	if(this.Buscar(e)){
		
		LinkedList<Integer> este=this.toList();
		este.remove(este.indexOf(e));
		Arbol a=toArbol(este);
		this.Raiz=a.Raiz;
		a=null;
		this.cant--;
	
	}
	
}
public static Arbol toArbol(LinkedList<Integer> lista){
	Arbol nogal=new Arbol();
	for(int x=0;x<lista.size();x++){
		nogal.Agregar(lista.get(x));
	}
	
	return nogal;
	
}




public LinkedList<Integer> toList(){
	
	LinkedList<Integer> lista=new LinkedList<Integer>();
	lista.add(Raiz.Carga);
	this.Raiz.toList(lista);
	return lista;
	
	
} 


public void Eliminar(int load){
	if (this.Raiz.Carga==load){
		this.Raiz=Nodo.ReorganizarHijos(this.Raiz);
		this.cant-=1;
	}
	else{
	this.Raiz.Eliminar(load);
	this.cant-=1;
	}
	
}

	
public void Agregar (int load){
	this.cant+=1;
	Nodo nodito=new Nodo(load);
	if (this.Raiz==null){
		this.Raiz=nodito;
		
		return;
	}
	this.Raiz.Agregar(nodito);
	
}
	
public void Imprimir(){
	this.Raiz.Imprimir();
	
}
	
	
}
