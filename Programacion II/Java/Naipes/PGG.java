import java.util.Scanner;
import java.util.Random;

public class PGG {

	public Mazo mazo;
	public int cantVecesJugadas;  //lleva cuenta de las manos jugadas
	public int maxCartas;		  //cant maxima de cartas del mazo
	public int cantCartas; 		  //la cant de cartas que tiene el mazo en algun momento, no debe superar a maxCartas
	public Jugador jugador;		  //el jugador solo se define en las subbarajas, no en el mazo "principal"
	
	
	/*este constructor toma de parametro un float ya que si el usuario ingresa una cantidad de
	* punto flotante, el programa no tira un error, solo que ingrese un numero hasta que
	* sea par entre 2 y 50 incluidos, se ve en metodo escanearInt2().El booleano se emplea para 
	* determinar si la instancia de PGG es un submazo, de ser asi, admite numeros pares e impares.
	*/
	public PGG(float n,boolean submazo){ 
		this.mazo = new Mazo();
		maxCartas = (int) n;
	}
	
	public Integer cantManosJugadas(){
		return this.cantVecesJugadas;
	}
	
	/*este metodo obtiene un float definido, asi se muestra de entrada el mensaje al usuario
	* y hasta que el mismo no ingrese numeros pares entre 2 y 50 enteros, va a mostrar el mismo
	* mensaje
	*/
	public static int escanearInt2(){
		float m = -1;
		while(m <= 0 || m > 50 || m%2 != 0 || m%1 != 0){
			System.out.println("Ingrese un numero entero par entre 2 y 50:");
			Scanner nuevo = new Scanner(System.in);
			m=nuevo.nextFloat();
		}
		int n = (int)m;
		return n;
	}
	
	//este metodo se emplea en la clase Test1(para elegir si emplear el test 1 o 2) 
	public static int escanearInt(){
		int m = 0;
		System.out.println(":");
		Scanner nuevo = new Scanner(System.in);
		m = nuevo.nextInt();
		return m;
	}
	
	/*agrega un naipe arriba del mazo con un 0, 1 o 2. Si la cant de cartas es igual a la cant maxima del mazo
	* no se agrega un naipe
	*/   
	public void agregarNaipe(Integer num){
		if (num > 2)
			throw new RuntimeException("Solo se permiten tres tipos: 0 (Perro), 1 (Gallina) y 2 (Gato)");
		if(cantCartas == maxCartas)
			throw new RuntimeException ("No se pueden agregar mas cartas");
		else{
			this.mazo.agregarArriba(num);
			this.cantCartas++;
		}
	}
	/*Este metodo se encarga de asignar valores a las cartas de manera aleatoria, es decir, 
	 * que el mazo puede contener cualquier cantidad de los tres valores (dentro del rango maximo
	 * de cartas).
	 */
	public void AsignarCartas(){
		while(this.cantCartas < this.maxCartas){
			Random ran = new Random();
			int rand=ran.nextInt(3);
			this.agregarNaipe(rand);
		}	
	}
	
	//lo mismo que agregar naipe solo que al fondo de la baraja
	public void agregarAlFondo(Integer num){
		if(num > 2)
			throw new RuntimeException ("Solo se permiten tres tipos: 0 (Perro), 1 (Gallina) y 2 (Gato)");
		if(cantCartas == maxCartas)
			throw new RuntimeException ("No se pueden agregar mas cartas");
		else{
			this.mazo.agregarAbajo(num);
			this.cantCartas++;
		}
	}
	
	/*quita la carta del tope de la baraja
	 * emplea la clase jugador ya que se llama principalmente sobre 
	 * subbarajas pertenecientes a los jugadores.
	 */
	public Integer roboCarta(){
		if( cantCartas <= 0) 
			throw new RuntimeException("No hay mas cartas que sacar");
		else{ 
			this.cantCartas--;
			this.jugador.Jugada=this.mazo.quitarCarta();  //devuelve el int de la carta que robo y la guarda enel jugador	
		return this.jugador.Jugada;
		}
	}
	
	/*Este metodo crea 2 nuevos pgg
	 * se recorre el mazo desde 0, hasta la mitad; y agrega la carta del mazo a la baraja
	 * el mazo se queda si cartas (cantCartas == 0), y en las barajas menores:
	 * 		la cantidad maxima de cartas es la del mazo original, ya que el ganador se queda con todas las cartas del mazo 
	 * 		la cantidad de cartas que poseen al crearse es la del mazo (parametro cantMazo) dividido 2 
	 */
	public PGG barajar(Integer desde, Integer hasta, Integer cantMazo){
		Integer cant = cantMazo/2;
		PGG sub = new PGG(cant,true);
		sub.maxCartas = cantMazo;   
		for(int x = desde; x < hasta; x++){
			Integer carta = this.roboCarta();
			sub.agregarNaipe(carta);
		}
		return sub;	
	}
	//este metodo se emplea en las subbarajas,
	//cuando la cantidad de cartas del jugador es 0 se devuelve el nombre del jugador
	public static String ganador(Jugador J1, Jugador J2){
		String ganador = "";
		if (J1.MazoJ.cantCartas == 0)
			ganador = J1.nombre;
		else if((J2.MazoJ.cantCartas == 0)){
			ganador = J2.nombre;
		}
		return ganador;
	}
	//muestra un resumen del juego en cada jugada
	public void resumen(PGG baraja1, PGG baraja2, Integer carta1, Integer carta2){
		String[] pgg = new String[3];
		pgg[0] = "Perro";
		pgg[1] = "Gato";
		pgg[2] = "Gallina";
	
		System.out.println( "              PGG           Turno= "+this.cantManosJugadas() );
		System.out.println( "Jugador 1, cartas retantes "+": "+(baraja1.cantCartas) );                
		System.out.println( "Jugador 2, cartas restantes"+": "+(baraja2.cantCartas) );                    
		System.out.println( "------------------------------------------" );
		System.out.println( "--------Jugador 1: "+pgg[carta1]+"------------------" );
		System.out.println( "------------------------------------------" );
		System.out.println( "--------Jugador 2: "+pgg[carta2]+"------------------" );
		System.out.println( "------------------------------------------" );
	}
	
	/*se aplica sobre las subbarajas
	 * representa una jugada del juego, el que pierde esa mano, se lleva las dos cartas jugadas
	 */
	public static void jugar(PGG mazo, PGG baraja1,PGG baraja2){
		int carta1 = baraja1.roboCarta();  
		int carta2 = baraja2.roboCarta();
		
		if(carta1 == carta2){
			System.out.println("Empate, se devuelven las cartas al fondo del mazo");
			baraja1.agregarAlFondo(carta1);
			baraja2.agregarAlFondo(carta2);
		}
		else{
			if (Math.abs(carta1 - carta2)>1){  
				if (carta1 < carta2){
					System.out.println("Jugador 2 gana, el jugador 1 se lleva las 2 cartas");
					baraja1.agregarAlFondo(carta1);
					baraja1.agregarAlFondo(carta2);
				}
				else if (carta1 > carta2 ){
					System.out.println("Jugador 1 gana, el jugador 2 se lleva las 2 cartas");
					baraja2.agregarAlFondo(carta1);
					baraja2.agregarAlFondo(carta2);
				}
			}
			else{
				if (carta1 < carta2){
					System.out.println("Jugador 1 gana, el jugador 2 se lleva las 2 cartas");
					baraja2.agregarAlFondo(carta1);
					baraja2.agregarNaipe(carta2);
				}
				else{
					System.out.println("Jugador 2 gana, el jugador 1 se lleva las 2 cartas");
					baraja1.agregarAlFondo(carta1);
					baraja1.agregarNaipe(carta2);
				}
			}
		}
		mazo.cantVecesJugadas++;
			
		//LLAMA A RESUMEN QUE IMPRIME PANTALLA
		
		mazo.resumen(baraja1, baraja2, carta1 , carta2);
	}
}
