
public class ListaInt {
	//Este es la estructura principal del programa dado que el TAD mazo se implementa 
	//sobre esta lista, contiene una referencia al primer y ultimo nodo de la lista 
	//para que el agregado y quitado de elementos sea mas eficiente: Complejidad de tiempo
	//contstante ( O(1) ). 
	
	private NodoInt primero;
	private NodoInt ultimo;
	NodoInt iterador;		//se utiliza para iterar sobre los elementos de la lista
	private int longitud;	//determina la cantidad de nodos de la lista.
	
	public ListaInt(){
		this.primero=null;
		this.ultimo=null;
		this.longitud=0;
		
	}
	public int largo() {
		return this.longitud;
	}
	//agrega adelante de la lista un nodo nuevo
	//si la lista esta vacia se asigna este nodo como el primero y el ultimo de la misma
	public void agregarAdelante(int x){
		NodoInt nuevo=new NodoInt(x);
		//si esta vacia
		if(this.primero==null || this.ultimo==null){ 
			this.primero=nuevo;
			this.ultimo=nuevo;
			this.longitud++;
		}
		else{
			this.primero.setAnterior(nuevo);
			nuevo.setSiguiente(this.primero);
			this.primero=nuevo;
			this.longitud++;
		}
	}
	/*
	 * Funciona de la mimsma manera que agregar adelante, solo que este elemento agrega el nodo
	 * al final de la lista.
	 */
	public void agregarAtras(int x){
		NodoInt nuevo=new NodoInt(x);
		//si esta vacia
		if(this.ultimo==null || this.primero==null){  //IDEM
			this.ultimo=nuevo;
			this.primero=nuevo;
			this.longitud++;
		}
		else{
			this.ultimo.setSiguiente(nuevo);
			nuevo.setAnterior(this.ultimo);
			this.ultimo=nuevo;
			this.longitud++;
		}
	}
	//quita el ultimo nodo de la lista.
	public void quitarUltimo(){
		NodoInt anteultimo= this.ultimo.getAnterior();  
		anteultimo.setSiguiente(null);
		this.longitud--;
	}
	//quita el primer nodo de la lista.
	public int quitarPrimero() {
		int elemento = this.primero.getElemento();    
		this.primero=this.primero.getSiguiente();
		this.longitud--;
		return elemento;
	}
	
	
	public boolean estaVacia(){
		return this.primero==null;
	}
	
//--------------------------------------------------------------------------------------	
	//estas funciones se emplean sobre el iterador de la lista.
	public void comenzar(){
		this.iterador=this.primero;
	}
	
	public void avanzar(){
		this.iterador=this.iterador.getSiguiente();
	}
	
	public int elementoActual(){
		return this.iterador.getElemento();
	}
	
	public void retroceder(){
		this.iterador=this.iterador.getAnterior();
	}
	
	public boolean termino(){
		return this.iterador==null;
	}
	
	public void imprimirLista(){
		this.comenzar();
		System.out.print("(");
		while(!this.termino()){
			// si es el ultimo que se imprima sin espacios
		   if(this.iterador.getSiguiente()==null){
			  System.out.print(this.elementoActual());
			  this.avanzar(); 
		   }
		   else{
			  System.out.print(this.elementoActual()+",");
			  this.avanzar();
		   }
		}
		
		System.out.println(")");
	}
	
//--------------------------------------------------------------------------------------------
	
}
