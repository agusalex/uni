package parte2;

import java.io.IOException;
import java.util.List;

public class ExportadorCSV extends Exportador
{

	private final String formato = ".csv";

	public ExportadorCSV(String Nombre) {
		super(Nombre);
	}

	public String getNombre() {
		return nombre;
	}

	@Override
	public void exportarArchivo(List<? extends Exportable> objetos) {
		try {
			boolean seenFirst = false;
			for (Exportable e : objetos) {
				Atributos attrs = e.extraerAtributos();
				if (!seenFirst) {
					seenFirst = true;
					for (String columna : attrs.keySet()) {
						printer.print(columna + ",");
					}
					printer.println();
				}
				// Imprimir la línea con los atributos.
				for (String val : attrs.values()) {
					printer.print(val + ",");
				}
				printer.println();
			}
			file.close();
		} catch (IOException e) {
			System.err.println("No se pudo realizar la copia");
		}
	}

	@Override
	public String getFormato() {
		return formato;
	}

}
