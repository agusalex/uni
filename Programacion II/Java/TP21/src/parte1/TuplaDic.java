package parte1;

/**
 * Tupla inmutable de dos elementos, para uso con DiccConjunto.
 *
 * equals() y compareTo() solo se fijan en el primer elemento.
 */

public class TuplaDic<T1 extends Comparable<T1>, T2> implements Comparable<TuplaDic<T1, T2>>
{

	private final T1 e1;
	private final T2 e2;

	public TuplaDic(T1 a, T2 b) {
		e1 = a;
		e2 = b;
	}

	public T1 getE1() {
		return e1;
	}

	public T2 getE2() {
		return e2;
	}

	/*
	 * IMPLEMENTAR LOS SIGUIENTES MÉTODOS.
	 */

	@Override
	public String toString() {

		return "(" + this.e1 + "," + this.e2 + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == null || obj == null) {
			if (this == null && obj == null) {
				return true;
			}
			else if (this == null && obj != null) {
				return false;
			}
			else {
				return false;
			}
		}

		if (obj instanceof TuplaDic) {

			TuplaDic<T1, T2> nueva = (TuplaDic<T1, T2>) obj;
			if (nueva.e1 != null && this.e1 != null) {
				if (nueva.e1.equals(this.e1)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int compareTo(TuplaDic<T1, T2> tupla) {
		if (this == null || tupla == null) {
			throw new RuntimeException("No se puede comparar nulls");
		}

		return this.e1.compareTo(tupla.e1);

	}
}
