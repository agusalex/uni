package dao.intento4.dao;

import java.util.ArrayList;

import dao.dm.Cliente;

public interface ClienteDAO extends DAO<Cliente>{

	 public int cantidadClientes();
	 public ArrayList<Cliente> buscarporRazonSocial(String rs);

}
