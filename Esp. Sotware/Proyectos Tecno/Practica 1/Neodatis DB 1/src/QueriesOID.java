import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;
import org.neodatis.odb.OID;

/**
 * Created by Max on 3/24/2017.
 */
public class QueriesOID {
    public static void main(String[] args) {
        ODB odb = null;
        try {
// Abrimos la base
            odb = ODBFactory.open("clase1db");
//creo un cliente
            Cliente c = new Cliente(300000, "Nuevo cliente");
//guardo
            OID idObjeto = odb.store(c);
//mando los cambios a la base.
            odb.commit();
//y ahora borro el objeto que tenía ese id.
            odb.deleteObjectWithId(idObjeto);
        } finally {
            if (odb != null) {
// cerramos la bd
                odb.close();
            }
        }
    }
}
