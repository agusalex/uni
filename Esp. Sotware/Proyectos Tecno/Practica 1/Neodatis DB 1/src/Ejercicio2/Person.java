package Ejercicio2;

/**
 * Created by Max on 3/24/2017.
 */
public class Person implements Keepable {

    private String name, surname;
    private String dayOfBirth;
    private Adress adress;

    public Person(String name, String surname, String dayOfBirth, Adress adress){
        this.name = name;
        this.surname = surname;
        this.dayOfBirth = dayOfBirth;
        this.adress = adress;
    }

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getSurname() {return surname;}

    public void setSurname(String surname) {this.surname = surname;}

    public Adress getAdress() {return adress;}

    public void setAdress(Adress adress) {this.adress = adress;}

    public String getDayOfBirth() {return dayOfBirth;}

    public void setDayOfBirth(String dayOfBirth) {this.dayOfBirth = dayOfBirth;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        if (surname != null ? !surname.equals(person.surname) : person.surname != null) return false;
        if (dayOfBirth != null ? !dayOfBirth.equals(person.dayOfBirth) : person.dayOfBirth != null) return false;
        return adress != null ? adress.equals(person.adress) : person.adress == null;

    }


}
