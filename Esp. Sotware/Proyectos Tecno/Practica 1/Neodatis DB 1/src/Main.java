import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;

/**
 * Created by Max on 3/24/2017.
 */
public class Main {
    public static void main(String[] args) {
        ODB odb = null;
        try {
            // Creamos la instancia
            Cliente c = new Cliente(17, "Carniceria Lola");
            System.out.println(c.getClass());
            // Abre/Crea la bd
            odb = ODBFactory.open("clase1db");
            // Guardamos el objeto
            odb.store(c);
        } finally {
            if (odb != null) {
                // Cerramos la bd
                odb.close();
            }
        }
    }
}
