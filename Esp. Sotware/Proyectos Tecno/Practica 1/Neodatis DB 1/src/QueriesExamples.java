import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;
import org.neodatis.odb.Objects;
import org.neodatis.odb.core.query.IQuery;
import org.neodatis.odb.core.query.criteria.Where;
import org.neodatis.odb.impl.core.query.criteria.CriteriaQuery;

/**
 * Created by Max on 3/24/2017.
 */
public class QueriesExamples {

    public static void main(String[] args) {
        ODB odb = null;
        try {
// Abrimos la bd
            odb = ODBFactory.open("clase1db");
            IQuery query = new CriteriaQuery(Cliente.class,
                    Where.like("razonSocial", "Nuevo cliente"));
            Objects<Cliente> clientes = odb.getObjects(query);
// recuperamos el objeto a borrar
            if(clientes.size() > 0){
                Cliente tito = (Cliente) clientes.getFirst();
                System.out.println(tito);
                odb.delete(tito);
            }
// y lo borramos
        } finally {
            if (odb != null) {
// siempre cerramos la bd.
                odb.close();
            }
        }
    }
}
