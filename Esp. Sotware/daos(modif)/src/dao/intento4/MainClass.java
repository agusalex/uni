package dao.intento4;

import DataObjects.Adress;
import DataObjects.Person;
import DataObjects.Province;
import dao.intento4.dao.PersonaDAO;
import dao.intento4.daos.impl.PersonDaoNeoDatis;
import org.neodatis.odb.core.query.IQuery;
import org.neodatis.odb.core.query.criteria.Where;
import org.neodatis.odb.core.query.nq.SimpleNativeQuery;
import org.neodatis.odb.impl.core.query.criteria.CriteriaQuery;

public class MainClass {

	public static void main(String[] args) {
		Province bsAs = new Province ("Buenos Aires");
		Adress dir1 = new Adress.AdressBuilder().province(bsAs).
				locality("Villa de Mayo").street("Rawson").
				streetNumber(3000).postCode(1615).department(0).build();
		Person person1 = new Person("Jorge", "Lopez", "05/05/1995",dir1,20);

		Province staFe = new Province ("Santa Fe");
		Adress dir2 = new Adress.AdressBuilder().province(staFe).
				locality("Los Santos").street("Santa").
				streetNumber(3000).postCode(2134).department(0).build();
		Person person2 = new Person("Juanito", "Sanchez", "10/02/1996",dir2,23);

		Province rioja = new Province ("La Rioja");
		Adress dir3 = new Adress.AdressBuilder().province(rioja).
				locality("Rojos").street("Rioja").
				streetNumber(3000).postCode(2134).department(0).build();
		Person person3 = new Person("Marcos", "Gonzalez", "14/5/2000",dir3,34);

		Province bsAs2 = new Province ("Buenos Aires");
		Adress adress1 = new Adress.AdressBuilder().province(bsAs).
				locality("Don Torcuato").street("Estrada").
				streetNumber(3000).postCode(2134).department(0).build();
		Person person4 = new Person("Juancho", "Suarez", "4/12/1990",adress1,30);

		PersonaDAO dataBasePerson = new PersonDaoNeoDatis();
		/*
		dataBasePerson.save(person1);
		dataBasePerson.save(person2);
		dataBasePerson.save(person3);
		dataBasePerson.save(person4);
		*/
		IQuery query  = new CriteriaQuery(Person.class, Where.like("name", "Juan"));
		System.out.println(dataBasePerson.getUsingQuery(query));
		IQuery query2 = new SimpleNativeQuery() {
			public boolean match(Person p) {
				return p.getName().toLowerCase().startsWith("j");
			}
		};
		query2.orderByAsc("surname,name");
		System.out.println();
		System.out.println(dataBasePerson.getUsingQuery(query2));


		IQuery query3 = new CriteriaQuery(Person.class, Where.and().add(Where.ge("edad",25)).
																	add(Where.le("edad",30)));
		System.out.println(dataBasePerson.getUsingQuery(query3));



	}

}
