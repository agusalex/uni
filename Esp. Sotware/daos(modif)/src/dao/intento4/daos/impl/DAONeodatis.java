package dao.intento4.daos.impl;

import dao.intento4.dao.DAO;
import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;
import org.neodatis.odb.Objects;
import org.neodatis.odb.core.query.IQuery;

public class DAONeodatis<T> implements DAO<T>{

	
	public void save(T t){
		ODB odb  = ODBFactory.open("mibase");
		odb.store(t);
		odb.close();
	}
	
	public void eraseAll(T t){
		ODB dataBase = null;
		try{
			dataBase = ODBFactory.open("mibase");
			Objects<T> objects = dataBase.getObjects(t.getClass());

			for (T object: objects)
				dataBase.deleteCascade(object);
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			if(dataBase != null)
				dataBase.close();
		}
	}


	public Objects<T> getUsingQuery(IQuery query) {
		ODB dataBase = null;
		Objects<T> objects = null;
		try{
			dataBase = ODBFactory.open("mibase");
			objects = dataBase.getObjects(query);

		}catch (Exception e){
			e.printStackTrace();
		}finally {
			if(dataBase != null)
				dataBase.close();
		}
		return objects;
	}


}
